# Class X Practice Papers

This is an unofficial set of the official previous-year examination papers and sample papers for the Indian CBSE Class X Board Examination.

## Downloading

Use the download button, the cloud-like symbol with the downward facing arrow, to download the entire set as a ZIP file.

Alternatively, you can go into each folder by clicking on them and download a specific file or folder by clicking on them and then on the download button.

## Contributing
Pull requests from Gitlab accounts are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update the exam papers as appropriate.